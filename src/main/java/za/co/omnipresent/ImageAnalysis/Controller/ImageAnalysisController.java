package za.co.omnipresent.ImageAnalysis.Controller;

import com.google.cloud.vision.v1.AnnotateImageResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
//import springfox.documentation.annotations.ApiIgnore;
import za.co.omnipresent.ImageAnalysis.Service.ImageAnalysisService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/image")
public class ImageAnalysisController {

    private ImageAnalysisService imageAnalysisService;

    @GetMapping(value = "/analysis/{imgPath}" )
    public @ResponseBody List<AnnotateImageResponse> scanImage(@PathVariable String imgPath) throws Exception{
        return imageAnalysisService.scanImage(imgPath);
    }

    @Autowired
    public void setImageAnalysisService(ImageAnalysisService imageAnalysisService) {
        this.imageAnalysisService = imageAnalysisService;
    }
}