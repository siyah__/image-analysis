package za.co.omnipresent.ImageAnalysis.Service;

import com.google.cloud.vision.v1.AnnotateImageResponse;

import java.io.IOException;
import java.util.List;

public interface ImageAnalysisService {
    public List<AnnotateImageResponse> scanImage(String imgPath) throws IOException;
}
