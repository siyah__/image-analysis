package za.co.omnipresent.ImageAnalysis.Service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.protobuf.ByteString;
import org.springframework.beans.factory.annotation.Value;

public class ImageAnalysisServiceImpl implements ImageAnalysisService {

    @Value("${image.path}")
    private String imagePath;

    public List<AnnotateImageResponse> scanImage(String imgPath) throws IOException {

        List<AnnotateImageRequest> requests = new ArrayList<>();

        ByteString byteString = ByteString.readFrom(new FileInputStream(imagePath+imgPath));
        Image img = Image.newBuilder().setContent(byteString).build();

        Feature feature = Feature.newBuilder().setType(Type.SAFE_SEARCH_DETECTION).build();
        AnnotateImageRequest annotateImageRequest = AnnotateImageRequest.newBuilder().addFeatures(feature).setImage(img).build();
        requests.add(annotateImageRequest);

        ImageAnnotatorClient imageAnnotatorClient = ImageAnnotatorClient.create();
        BatchAnnotateImagesResponse batchAnnotateImagesResponse = imageAnnotatorClient.batchAnnotateImages(requests);

        return batchAnnotateImagesResponse.getResponsesList();

    }
}
