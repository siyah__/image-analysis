package za.co.omnipresent.ImageAnalysis.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.omnipresent.ImageAnalysis.Service.ImageAnalysisService;
import za.co.omnipresent.ImageAnalysis.Service.ImageAnalysisServiceImpl;

@Configuration
public class BeanConfig {

    @Bean
    public ImageAnalysisService imageAnalysisService(){return new ImageAnalysisServiceImpl();
    }
}
